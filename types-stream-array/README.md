# Installation
> `npm install --save @types/stream-array`

# Summary
This package contains type definitions for stream-array (https://github.com/mimetnet/node-stream-array).

# Details
Files were exported from https://github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/stream-array

Additional Details
 * Last updated: Tue, 09 Oct 2018 17:57:22 GMT
 * Dependencies: node
 * Global values: none

# Credits
These definitions were written by Tyler Murphy <https://github.com/Tyler-Murphy>.
